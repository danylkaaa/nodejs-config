# config

[![pipeline status](https://gitlab.com/ZulusK/nodejs-config/badges/master/pipeline.svg)](https://gitlab.com/ZulusK/nodejs-config/commits/master)
[![coverage report](https://gitlab.com/ZulusK/nodejs-config/badges/master/coverage.svg)](https://gitlab.com/ZulusK/nodejs-config/commits/master)

Validator for configuration files, based on Joi, loads configuration, based on `process.env.NODE_ENV` property

```bash
npm i zulus/config
```

### Structure

- [API](#api)
- [Usage](#usage)
  - [Index file](#index-config-file)
  - [Sample config schema](#sample-schema)
- [Contributing rules](#contributing)

<a name="usage"></a>

### USAGE

<a name="index-config-file"></a>

#### Index config file

```js
// config/index.js
const config = require('@dev/config');
const schema = require('./configSchema');
module.exports = config.loadConfig(__dirname, schema, null, 'config.#env');
```

<a name="sample-schema"></a>

#### Sample schema for validation

```js
// configSchema.js
const Joi = require('joi');
module.exports = Joi.object({
  mysql: Joi.object({
    connectionLimit: Joi.number().min(1),
    host: Joi.string().trim(),
    port: Joi.number().min(1),
    user: Joi.string(),
    password: Joi.string(),
    database: Joi.string(),
    debug: Joi.boolean()
      .default(false)
      .optional()
  }).strict(true),
  aerospike: Joi.object({
    hosts: Joi.array().items(
      Joi.object({
        addr: Joi.string(),
        port: Joi.number().min(1)
      })
    ),
    log: Joi.object({
      level: Joi.any(),
      file: Joi.string()
    }).optional(),
    user: Joi.string().optional(),
    password: Joi.string().optional(),
    policies: Joi.object({}).optional(),
    modlua: Joi.object({
      systemPath: Joi.string(),
      userPath: Joi.string()
    }).optional(),
    maxConnsPerNode: Joi.number().min(1)
  }).strict(true)
});
```

<a name="api"></a>

### API

### `config.loadConfig(configDir, schema, options, pattern)`

Loads configuration from file, located in `configDir` based on current NODE_ENV value.
May throw an error, if validation failed.

- `configDir` - path to directory, which contains all configuration files
- `schema` - Joi schema for validation of all config files, see [Joi API](https://github.com/hapijs/joi/blob/v14.0.3/API.md)
- `[options]` - Custom Joi options for validation, see [API](https://github.com/hapijs/joi/blob/v14.0.3/API.md#validatevalue-schema-options-callback)
  - `[allowUnknown]` - allow unknown properties (default `true`)
  - `[convert]` - try to convert string -> numbers
  - `[presence]` - default behaviour of validation, default `required
- `[pattern]` - pattern of config files, default `config.#env`. Note, chain `#env` will be replaced by current NODE_ENV value

<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:ZulusK/nodejs-config.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-logger`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-logger`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   ```bash
   npm run lint
   ```
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc
[docs-dir]: docs/overview.md
