require('dotenv')
  .config();
const Joi = require('@hapi/joi');
const path = require('path');
const debug = require('debug')('@dev/config');

const DEFAULT_JOI_OPTIONS = {
  allowUnknown: true,
  convert: true,
  presence: 'required'
};
const DEFAULT_CONFIG_FILENAME_PATTERN = 'config.#env';
/**
 * Loads testData from specified directory, and validates it by providing schema
 * @param {string} configDir - directory with config files
 * @param {object} validationSchema - schema for validation of testData
 * @param {object} options - custom options for Joi
 * @param {string} pattern - patter for names of config files
 */
module.exports = (configDir, validationSchema, options = {}, pattern = DEFAULT_CONFIG_FILENAME_PATTERN) => {
  const env = process.env.NODE_ENV || 'development';
  debug('Use %s end', env);
  const configFile = path.join(configDir, pattern.replace('#env', env));
  debug('Import config file %s', configFile);
  const configBasedOnEnv = require(configFile);
  const { error, value: validatedConfig } = Joi.validate(configBasedOnEnv, validationSchema, Object.assign({ ...DEFAULT_JOI_OPTIONS }, options));
  if (error) {
    throw new Error(`Config validation error: ${error.message}`);
  }
  return validatedConfig;
};
