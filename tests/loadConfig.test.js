const { expect } = require('chai');
const path = require('path');
const loadConfig = require('../lib/loadConfig');

describe('LoadConfig', () => {
  it('should validate config for test mode', testValidateInTestMode);
  it('should throw an error, no config dir', testNoConfigDir);
  it('should throw an error, not valid config', testNotValidConfig);
});
const Joi = require('joi');

const SCHEMA = Joi.object().keys({
  required: Joi.string(),
  optional: Joi.string().optional()
});
const INVALID_SCHEMA = Joi.object().keys({
  required: Joi.string(),
  optional: Joi.string()
});

function testValidateInTestMode() {
  const validatedConfig = loadConfig(path.join(__dirname, '../resources'), SCHEMA);
  expect(validatedConfig).to.be.deep.eq({ required: '42' });
}

function testNoConfigDir() {
  expect(() => loadConfig(null, SCHEMA)).to.throw();
}

function testNotValidConfig() {
  expect(() => loadConfig(path.join(__dirname, 'testData'), INVALID_SCHEMA)).to.throw();
}
