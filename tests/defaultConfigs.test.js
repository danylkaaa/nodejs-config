const { expect } = require('chai');
const defaultConfigs = require('../lib/defaultConfigs');
const Joi = require('joi');

describe('DefaultConfigs', () => {
  describe('Logger', testLoggerConfig);
  describe('Aerospike', testAerospikeConfig);
  describe('MySQL', testMySQL);
});

function testLoggerConfig() {
  it('should return Joi schema', () => {
    expect(defaultConfigs.logger.generate().isJoi)
      .to
      .be
      .eql(true);
  });
  it('should return valid Joi schema for logger (fails with invalid arguments)', () => {
    const schema = defaultConfigs.logger.generate();
    const value = {};
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .not
      .be
      .eq(null);
  });
  it('should return valid Joi schema for logger (pass with valid arguments)', () => {
    const schema = defaultConfigs.logger.generate();
    const value = {
      transports: [
        {
          type: 'console',
          level: 'debug'
        }
      ]
    };
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .be
      .eq(null);
  });
}

function testAerospikeConfig() {
  it('should return Joi schema', () => {
    expect(defaultConfigs.aerospike.generate().isJoi)
      .to
      .be
      .eql(true);
  });
  it('should return valid Joi schema for aerospike (fails with invalid arguments)', () => {
    const schema = defaultConfigs.aerospike.generate();
    const value = {};
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .not
      .be
      .eq(null);
  });
  it('should return valid Joi schema for aerospike (pass with valid arguments)', () => {
    const schema = defaultConfigs.aerospike.generate();
    const value = {
      hosts: [{
        addr: '127.0.0.1',
        port: '3000'
      }],
      maxConPerNode: 10,
      readPolicy: {
        totalTimeout: 3000
      },
      writePolicy: {
        totalTimeout: 3000
      }
    };
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .be
      .eq(null);
  });
}

function testMySQL() {
  it('should return Joi schema', () => {
    expect(defaultConfigs.mysql.generate().isJoi)
      .to
      .be
      .eql(true);
  });
  it('should return valid Joi schema for mysql (fails with invalid arguments)', () => {
    const schema = defaultConfigs.mysql.generate();
    const value = {};
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .not
      .be
      .eq(null);
  });
  it('should return valid Joi schema for mysql (pass with valid arguments)', () => {
    const schema = defaultConfigs.mysql.generate();
    const value = {
      host: '127.0.0.1',
      port: '3006',
      user: 'user',
      password: 'password',
    };
    expect(Joi.validate(value, schema, { presence: 'required' }).error)
      .to
      .be
      .eq(null);
  });
}
