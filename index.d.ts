declare module '@zulus/config' {
    import Joi from "@hapi/joi";
    export function loadConfig<T>(
        configDir: string,
        validationSchema: Joi.SchemaLike,
        options: Joi.ValidationOptions,
        pattern: string): T
}